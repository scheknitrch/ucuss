import numpy as np


def f(x):
    return np.exp(-np.square(x) / 2) + np.sin(20 * x)


def sample(N):
    return np.random.uniform(size=N)


N = 10
estimate = []
for i in range(1000):
    f_values = f(sample(N))
    estimate.append(np.mean(f_values))
std = np.std(estimate)
mean = np.mean(estimate)
print(mean, std)