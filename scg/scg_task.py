import torch
import numpy as np


def f(x, c=4.):
    raise NotImplementedError


def sample_gaussian_noise(mu, log_sigma, size, reparameterise=False):
    # Sample Gaussian noise and return it.
    # Implement both parameterisable and non reparameterisable sampling.
    raise NotImplementedError


def log_prob(samples, mu, log_sigma):
    # Return Gaussian(mu, sigma) log prob of samples
    raise NotImplementedError


def get_params(mu_init, log_sigma_init):
    # Return mu and sigma parameters with initial values mu_init, log_sigma_init
    raise NotImplementedError


def sgd_step(w, lr):
    """
    w: model's parameter
    lr: learning rate
    Performs SGD update of the parameter `w` using `w.grad`
    """
    raise NotImplementedError


torch.manual_seed(42)

learning_rate = 0.001
batch_size = 2
max_num_updates = 30000


# Path-wise derivative estimator
mu, log_sigma = get_params(1.0, 0.0)
for i in range(max_num_updates):
    samples = sample_gaussian_noise(mu, log_sigma, batch_size, True)

    # implement loss for path-wise derivative estimator
    raise NotImplementedError

    loss.backward()
    sgd_step(mu, learning_rate)
    sgd_step(log_sigma, learning_rate)

print(mu.item())
print(log_sigma.exp().item())

# score function estimator
mu, log_sigma = get_params(1.0, 0.0)
for i in range(max_num_updates):
    samples = sample_gaussian_noise(mu, log_sigma, batch_size, False)

    # implement loss for score function estimator
    raise NotImplementedError

    loss.backward()
    sgd_step(mu, learning_rate)
    sgd_step(log_sigma, learning_rate)


print(mu.item())
print(log_sigma.exp().item())