import numpy as np
import matplotlib.pyplot as plt


n_cats = 7
cats = np.arange(n_cats)

probs = np.random.randint(low=1, high=20, size=n_cats)
probs = probs / sum(probs)
logits = np.log(probs)


def plot_probs():
    plt.bar(cats, probs)
    plt.xlabel("Category")
    plt.ylabel("Probability")


plt.figure()
plot_probs()
plt.show()


def plot_estimated_probs(samples):
    n_cats = np.max(samples) + 1
    estd_probs, _, _ = plt.hist(samples,
                                bins=np.arange(n_cats + 1),
                                align='left',
                                edgecolor='white',
                                density=True)
    plt.xlabel("Category")
    plt.ylabel("Estimated probability")
    return estd_probs


def sample_uniform(logits):
    noise = np.random.uniform(size=len(logits))
    sample = np.argmax(logits + noise)
    return sample


def sample_normal(logits):
    noise = np.random.normal(size=len(logits))
    sample = np.argmax(logits + noise)
    return sample


def sample_gumbel(logits):
    noise = np.random.gumbel(size=len(logits))
    sample = np.argmax(logits + noise)
    return sample


def print_probs(probs):
    print(" ".join(["{:.2f}"] * len(probs)).format(*probs))


for sample in [sample_uniform, sample_normal, sample_gumbel]:
    print(sample)

    n_samples = 5000
    samples = [sample(logits) for _ in range(n_samples)]

    plt.figure()
    plt.subplot(1, 2, 1)
    plot_probs()
    plt.subplot(1, 2, 2)
    estd_probs = plot_estimated_probs(samples)
    plt.tight_layout()

    print("Original probabilities:\t\t", end="")
    print_probs(probs)
    print("Estimated probabilities:\t", end="")
    print_probs(estd_probs)
    plt.show()
