import torch
import numpy as np
from torch.distributions.categorical import Categorical


def get_initial_w():
    """
    Returns 2x2 tensor
    """
    raise NotImplementedError


def model(x, w):
    """
    x: input
    w: weight
    Returns a sample from the categorical distribution, and the log probability of the sample.
    """

    raise NotImplementedError


def get_cost(y, y_hat):
    """
    y: variable that represents the true label
    y_hat: variable that represents predicted label
    Returns hamming distance
    """
    raise NotImplementedError


def get_score_function_surrogate_loss(cost, log_prob, baseline=None):
    """
    :param cost:
    :param log_prob:
    :param baseline:
    Return surrogate_loss for the score function estimator
    """
    raise NotImplementedError


def sgd_step(w, lr):
    """

    w: model's parameter
    w_grad: gradient of the loss function w.r.t. w
    lr: learning rate
    Performs SGD update of the parameter `w` using `w.grad`
    """
    w.data -= lr * w.grad.data
    raise NotImplementedError


N = 100
data = np.concatenate((np.random.normal(scale=0.25, size=(N, 2)) + np.array([-0.5, -0.5]),
                       np.random.normal(scale=0.25, size=(N, 2)) + np.array([0.5, 0.5]))).astype(np.float32)
labels = np.array(N * [0] + N * [1], dtype=np.int64)
w = get_initial_w()

# without baseline
epoch_loss_trace = []
for i in range(100):
    idxs = list(range(2 * N))
    np.random.shuffle(idxs)
    loss_trace = []
    for idx in idxs:
        x = torch.tensor(data[idx])
        label = torch.tensor(labels[idx])
        label_hat, log_prob = model(x, w)
        cost = get_cost(label, label_hat)
        loss = get_score_function_surrogate_loss(cost, log_prob)
        loss.backward()
        sgd_step(w, 0.01)
        loss_trace.append(cost.item())
    epoch_loss_trace.append(np.mean(loss_trace))
print(w)


# with baseline
epoch_loss_trace_b = []
exp_moving_average = 1
for i in range(100):
    idxs = list(range(2 * N))
    np.random.shuffle(idxs)
    loss_trace = []
    for idx in idxs:
        x = torch.tensor(data[idx])
        label = torch.tensor(labels[idx])
        label_hat, log_prob = model(x, w)
        cost = get_cost(label, label_hat)
        loss = get_score_function_surrogate_loss(cost, log_prob, baseline=exp_moving_average)
        loss.backward()
        sgd_step(w, 0.01)
        loss_trace.append(cost.item())
        # calculate moving average
        raise NotImplementedError
    epoch_loss_trace_b.append(np.mean(loss_trace))
print(w)


# ========================================================================
import matplotlib.pyplot as plt

plt.plot(epoch_loss_trace)
plt.plot(epoch_loss_trace_b)
plt.show()

plt.scatter(data[:, 0][:N], data[:, 1][:N], c="red", alpha=0.5)
plt.scatter(data[:, 0][N:], data[:, 1][N:], c="green", alpha=0.5)
plt.show()


